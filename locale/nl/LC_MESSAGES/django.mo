��    N      �  k   �      �  !  �  �  �  
  v	  �   �
    H  �   Z  5  8  b   n  �   �     f     �     �     �     �     �       7     3   S  +   �  (   �     �     �                 #   <     `     f     }     �  (   �     �  9   �  -        0     8     ?     O     i     �     �     �  "   �     �  :   �  .     $   D     i      �     �     �     �  !   �  7   �  +   &  @   R  ?   �  3   �       #   !  �   E  �   �  �   �  �   #  m   �  *   T          �  R   �  �   �  �   �  �   =  8   �  c   
  �   n  =   
     H  B  K    �  �  �     D   �   e!    7"  �   I#  4  .$  ]   c%  �   �%  !   U&     w&     �&  %   �&     �&     �&     '  9   )'  2   c'  -   �'  ,   �'     �'     (     (  
   /(     :(     T(  	   s(     }(  
   �(     �(  /   �(     �(  8   �(  ,    )     M)     \)     e)     z)     �)     �)     �)     �)  $   �)     �)  6   *  *   D*      o*     �*  !   �*     �*     �*  
    +  %   +  8   1+  ,   j+  9   �+  7   �+  +   	,  !   5,  -   W,  �   �,  �   =-  �   �-  �   j.  r   9/  4   �/  	   �/     �/  j   �/  �   h0  �   &1  �   �1  6   n2  r   �2  �   3  3   �3     4                  ;   D      ?              )   0              
   &      1             	   F   4   -   J   '          "       H   %      2          B           *   3          N   !              6   9   .   >                        =               7      ,       /       E       5      +   M   L             A       #      <      $           G       K           8   @   (   :   C            I          


Hi,

The user %(name_of_user_admin_role_changed)s is no longer a site administrator. This action was taken by %(user_name)s.
Check out the profile of the old site administrator here %(link)s.

If you believe this was done in error, please login here %(site_url)s and undo this action.

 

Dear %(name_deleted_user)s,<br><br> Your membership of <a href="%(site_url)s">%(site_url)s</a> has been cancelled by the administrator.<br>

Your profile, settings and saved items have been removed from the site and your content has been made anonymous.

Your Pleio account will continue to exist as will your membership of any other sites. See <a href="https://account.pleio.nl">account.pleio.nl</a> for more information.

 

Hi %(name_of_user_admin_role_changed)s,

You've been assigned as a new site administrator by %(user_name)s.

Enjoy your new found rights by logging in here %(site_url)s.

If you believe this was done in error, please contact one of the other site administrators.

 

Hi %(name_of_user_admin_role_changed)s,

Your site administrator role was removed by %(user_name)s.

If you believe this was done in error, please contact one of the other site administrators.",

 

Hi,

The user %(name_of_user_admin_role_changed)s was assigned as a new site administrator by %(user_name)s.
Check out the profile of the new site administrator here %(link)s .

If you believe this was done in error, please login here %(site_url)s and undo this action.

 
    
        Confirm your registration for %(title)s.<br />
        Location: %(location)s
        Date: %(start_date)s<br />
        Click on the link below to conirm. <br />
        <a href="%(link)s">%(link)s</a>
     
        This is confirmation of your registration for %(title)s.<br />
        Location: %(location)s
        Date: %(start_date)s<br />
        Click on the link below to see or make changes to your registration and to add this appointment to your agenda. <br />
        <a href="%(link)s">%(link)s</a>
     
Hi,

The administrator %(name_deleted_user)s is deleted. This action was taken by %(user_name)s.
  You have received this e-mail because you registered to receive notifications.<br /> Click <a href="%(user_url)s">here</a> to change your settings.  has begun a new Discussion:   has placed an Update:   has posted a Question:   has posted a new Agenda item:   has posted a new Blog:   has posted a new Wiki:   has responded at:  A new site administrator was assigned for %(site_name)s A site administrator was removed from %(site_name)s Access request for the %(group_name)s group Account of %(name_deleted_user)s removed Activity stream Agenda item created by Blog created by CMS page Confirmation of registration %s Confirmation of registration for %s Daily Don't have an account? Dutch English For all new messages visit %(site_name)s Go to %(site_name)s Invitation to become a member of the %(group_name)s group Invitation to become a member of the %s group Just me Log in Logged in users Login using Pleio account Message from group {0}: {1} Message from {0}: {1} Monthly Never New notifications at %(site_name)s News added on Ownership of the %(group_name)s group has been transferred Ownership of the %s group has been transferred Please click on the button to login. Please correct the error below. Please correct the errors below. Public Question asked by Register Regular overview of %(site_name)s Reminder to become a member of the %(group_name)s group Reminder to become a member of the %s group Request for access to the %(group_name)s group has been approved Request for access to the %(group_name)s group has been refused Request for access to the %s group has been refused Request to remove account Request to remove account cancelled The administrator %(user_name)s has approved your request to join the %(group_name)s group. Follow the link below to go directly to the group:<br /><a href="%(link)s">%(link)s</a>  The administrator %(user_name)s has rejected your request to join the %(group_name)s group. Contact the administrator for more information.  The administrator %(user_name)s has transferred the ownership of the group %(group_name)s to you. View the group at: <br /><a href="%(link)s">%(link)s</a>  The user %(user_name)s (%(user_obfuscated_email)s) has requested access to the group %(group_name)s. Follow the link below to become a member of the group: <br /><a href="%(link)s">%(link)s</a>  This message is sent from <a href="%(site_url)s">%(site_url)s</a> by <a href="%(user_url)s">%(user_name)s</a> This site is closed. Log in to get access. Weekly Wiki created by You are authenticated as %(username)s, but are not authorized to access this page. You have been invited again by %(user_name)s to join the %(group_name)s group. Follow the link below to become a member of the group: <br /><a href="%(link)s">%(link)s</a>  You have been invited by %(user_name)s to join the %(group_name)s group. Follow the link below to become a member of the group: <br /><a href="%(link)s">%(link)s</a>  You have received this e-mail because you registered to receive notifications.<br /> Click <a href="%(user_url)s">here</a> to change your settings. You're granted site administrator right on %(site_name)s You, as <strong> %(user_name)s </strong> user, have cancelled your request to remove your account.  You, as user, <strong>%(user_name)s</strong> have requested that your account be removed. You will be informed once the website administrator has done so.  Your site administrator rights for %(site_name)s were removed on Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 


Hallo,

De gebruiker %(name_of_user_admin_role_changed)s is niet langer een site beheerder. Dit is gedaan door  %(user_name)s.
Bekijk het profiel van de gebruiker hier: %(link)s.

Indien deze wijziging ongewenst is, kunt u zich hier %(site_url)s aanmelden om het ongedaan maken.

 

Beste %(name_deleted_user)s,<br><br> Je lidmaatschap van <a href="%(site_url)s">%(site_url)s</a>  is opgezegd door de beheerder.<br>

Je profiel, instellingen en bewaarde items zijn verwijderd van de site en jouw content is geanonimiseerd.

Jce Pleio-acount blijft bestaan evenals je lidmaatschap van eventuele andere sites.Zie <a href="https://account.pleio.nl">account.pleio.nl</a> voor meer informatie.

 

Hallo %(name_of_user_admin_role_changed)s,

U bent aangewezen als site beheerder door %(user_name)s.

U kunt gebruik maken van uw nieuwe rechten door u hier aan te melden: %(site_url)s.

Indien deze wijziging ongewenst is, kunt u contact opnemen met een van de andere site beheerders.

 

Hallo %(name_of_user_admin_role_changed)s,

Uw site beheerder rechten zijn afgenomen door %(user_name)s.

Indien deze wijziging ongewenst is, kunt u contact opnemen met een van de andere site beheerders.",

 

Hallo,

De gebruiker %(name_of_user_admin_role_changed)s is aangewezen als nieuwe site beheerder door %(user_name)s.
Bekijk het profiel van de gebruiker hier: %(link)s .

Indien deze wijziging ongewenst is, kunt u zich hier %(site_url)s aanmelden om het ongedaan maken.

 
    
        Bevestig je aanmelding voor %(title)s.<br />
        Locatie: %(location)s
        Datum: %(start_date)s<br />
        Klik op onderstaande link om te bevestigen. <br />
        <a href="%(link)s">%(link)s</a>
     
        Hierbij bevestigen we je aanmelding voor %(title)s.<br />
        Locatie: %(location)s
        Datum: %(start_date)s<br />
        Klik op de onderstaande link om je aanmelding te bekijken of te wijzigen en om de afspraak in je agenda te zetten.  <br />
        <a href="%(link)s">%(link)s</a>
     
Hallo,

De gebruiker %(name_deleted_user)s is verwijderd. Dit is gedaan door %(user_name)s.
  Je ontvangt deze e-mail omdat je aangemeld bent voor de notificaties.<br /> Klik <a href="%(user_url)s">hier</a> om je instellingen aan te passen.  is een nieuwe Discussie gestart   heeft een Update geplaatst   heeft een Vraag gesteld   heeft een nieuw Agenda-item gemaakt   heeft een nieuw Blog gemaakt   heeft een nieuwe Wiki gemaakt   heeft gereageerd op  Een nieuwe site beheerder is toegewezen aan %(site_name)s Een site beheerder is verwijderd van %(site_name)s Toegangsaanvraag voor de groep %(group_name)s Account verwijderd van %(name_deleted_user)s Activiteitenstroom Agenda-item gemaakt door Blog gemaakt door CMS pagina Bevestiging aanmelding %s Bevestiging aanmelding voor %s Dagelijks Nog geen account? Nederlands Engels Voor alle nieuwe berichten bekijk %(site_name)s Ga naar %(site_name)s Uitnodiging om lid te worden van de groep %(group_name)s Uitnodiging om lid te worden van de groep %s Alleen mijzelf Inloggen Ingelogde gebruikers Log in met Pleio account Bericht van groep {0}: {1} Bericht van {0}: {1} Maandelijks Nooit Nieuwe notificaties op %(site_name)s Nieuws geplaats op Eigenaarschap van de groep %(group_name)s overgedragen Eigenaarschap van de groep %s overgedragen Klik op de knop om in te loggen. Corrigeer de onderstaande fout. Corrigeer de onderstaande fouten. Iedereen publiek zichtbaar Vraag gesteld door Registreer Periodiek overzicht van %(site_name)s Herinnering om lid te worden van de groep %(group_name)s Herinnering om lid te worden van de groep %s Toegangsaanvraag voor de groep %(group_name)s goedgekeurd Toegangsaanvraag voor de groep %(group_name)s afgewezen Toegangsaanvraag voor de groep %s afgewezen Verzoek om account te verwijderen Verzoek om account te verwijderen geannuleerd De beheerder %(user_name)s heeft jouw aanvraag tot de groep %(group_name)s goedgekeurd. Volg de onderstaande link om direct naar de groep te gaan:<br /><a href="%(link)s">%(link)s</a> De beheerder %(user_name)s heeft jouw aanvraag tot de groep %(group_name)s afgewezen. Neem contact op met de beheerder voor meer informatie. De beheerder %(user_name)s heeft het eigenaarschap van de groep %(group_name)s aan jou overgedragen. Bekijk de groep op: <br /><a href="%(link)s">%(link)s</a>  De gebruiker %(user_name)s (%(user_obfuscated_email)s) heeft toegang aangevraagd tot de groep %(group_name)s. Volg de onderstaande link om lid te worden van de groep:  <br /><a href="%(link)s">%(link)s</a>  Dit bericht is verstuurd van <a href="%(site_url)s">%(site_url)s</a> door <a href="%(user_url)s">%(user_name)s</a> Deze site is besloten. Log in om toegang te krijgen. Wekelijks Wiki gemaakt door U bent geauthenticeerd als %(username) s, maar bent niet gemachtigd om toegang te krijgen tot deze pagina. Je bent nogmaals uitgenodigd door %(user_name)s om lid te worden van de groep %(group_name)s. Volg de onderstaande link om lid te worden van de groep: <br /><a href="%(link)s">%(link)s</a>  Je bent uitgenodigd door %(user_name)s om lid te worden van de groep %(group_name)s. Volg de onderstaande link om lid te worden van de groep: <br /><a href="%(link)s">%(link)s</a>  Je ontvangt deze e-mail omdat je aangemeld bent voor de notificaties.<br /> Klik <a href="%(user_url)s">hier</a> om je instellingen aan te passen. U bent aangewezen als site beheerder van %(site_name)s Je hebt als gebruiker <strong> %(user_name)s </strong> het verzoek om je account te laten verwijderen geannuleerd. Je hebt als gebruiker <strong>%(user_name)s</strong> verzocht aan de deelsitebeheerders om je account te laten verwijderen. Wanneer de deelsitebeheerder dit heeft doorgevoerd, ontvang je hierover bericht.  U bent niet langer site beheerder van %(site_name)s op 