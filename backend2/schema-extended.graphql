input deleteUserInput {
  guid: String!
}

input toggleUserIsAdminInput {
  guid: String!
}

input toggleUserIsBannedInput {
  guid: String!
}

type deleteUserPayload {
  success: Boolean
}

type toggleUserIsAdminPayload {
  success: Boolean
}

type toggleUserIsBannedPayload {
  success: Boolean
}

input inviteToSiteInput {

  """A list of email addresses to invite to site."""
  emailAddresses: [String!]

  """Message in email sent with invite"""
  message: String
}

type inviteToSitePayload {
  success: Boolean
}

input revokeInviteToSiteInput {

  """A list of email addresses to revoke invite for."""
  emailAddresses: [String!]
}

type revokeInviteToSitePayload {
  success: Boolean
}

type SiteInvite {
  email: String
}

type SiteInviteList {
  edges: [SiteInvite]
}


type SiteUserList {
  total: Int!
  edges: [User]
}

extend type Mutation {
  deleteUser(input: deleteUserInput!): deleteUserPayload
  toggleUserIsAdmin(input: toggleUserIsAdminInput!): toggleUserIsAdminPayload
  toggleUserIsBanned(input: toggleUserIsBannedInput!): toggleUserIsBannedPayload
  addSiteSettingProfileField(input: addSiteSettingProfileFieldInput!): addSiteSettingProfileFieldPayload
  editSiteSettingProfileField(input: editSiteSettingProfileFieldInput!): editSiteSettingProfileFieldPayload
  deleteSiteSettingProfileField(input: deleteSiteSettingProfileFieldInput!): deleteSiteSettingProfileFieldPayload
  """Create and send invitation by email to join a site."""
  inviteToSite(input: inviteToSiteInput!): inviteToSitePayload
  """Remove and send invitation by email to join a site."""
  revokeInviteToSite(input: revokeInviteToSiteInput!): revokeInviteToSitePayload

}

extend type Query {
  siteUsers(q: String, offset: Int, limit: Int, isAdmin: Boolean, isDeleteRequested: Boolean, isBanned: Boolean): SiteUserList
}

extend type User {
  isAdmin: Boolean
}


input addSiteSettingProfileFieldInput {
  key: String
  name: String!
  isEditable: Boolean
  isFilter: Boolean
  isInOverview: Boolean
  fieldType: String!
  fieldOptions: [String]
  isInOnboarding: Boolean
  isMandatory: Boolean
}

type addSiteSettingProfileFieldPayload {
  profileItem: ProfileItem
}

input editSiteSettingProfileFieldInput {
  guid: String!
  name: String
  isEditable: Boolean
  isFilter: Boolean
  isInOverview: Boolean
  fieldType: String
  fieldOptions: [String]
  isInOnboarding: Boolean
  isMandatory: Boolean
}

type editSiteSettingProfileFieldPayload {
  profileItem: ProfileItem
}

input deleteSiteSettingProfileFieldInput {
  guid: String!
}

type deleteSiteSettingProfileFieldPayload {
    success: Boolean
}

extend type ProfileItem {
  guid: String!
  isInOnboarding: Boolean
  isMandatory: Boolean
}

type ProfileSection {
  name: String!
  profileFieldGuids: [String]
}

extend input editSiteSettingInput {
  profileSections: [ProfileSectionInput]
  onboardingEnabled: Boolean
  onboardingForceExistingUsers: Boolean
  onboardingIntro: String
}

input ProfileSectionInput {
  name: String!
  profileFieldGuids: [String]
}
type ExportableField {
  field_type: String!
  field: String!
  label: String!
}

extend type Site {
  profileSections: [ProfileSection]
  onboardingEnabled: Boolean!
  onboardingIntro: String
}

extend type SiteSettings {
  profileSections: [ProfileSection]
  exportableUserFields: [ExportableField]
  profileFields: [ProfileItem]
  limitedGroupAdd: Boolean!
  onboardingEnabled: Boolean!
  onboardingForceExistingUsers: Boolean!
  onboardingIntro: String
  siteInvites: SiteInviteList
  cookieConsent: Boolean!
}

extend input editSiteSettingInput {
  limitedGroupAdd: Boolean
  cookieConsent: Boolean
}