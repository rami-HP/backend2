from django.db import connection
from django_tenants.test.cases import FastTenantTestCase
from backend2.schema import schema
from ariadne import graphql_sync
import json
from django.contrib.auth.models import AnonymousUser
from django.http import HttpRequest
from core.models import Group
from user.models import User
from ..models import Discussion
from core.constances import ACCESS_TYPE
from mixer.backend.django import mixer
from graphql import GraphQLError
from datetime import datetime

class EditEventTestCase(FastTenantTestCase):

    def setUp(self):
        self.anonymousUser = AnonymousUser()
        self.authenticatedUser = mixer.blend(User)
        self.editorUser = mixer.blend(User, is_admin=True) # TODO: update with roles

        self.discussionPublic = Discussion.objects.create(
            title="Test public event",
            description="Description",
            rich_description="JSON to string",
            read_access=[ACCESS_TYPE.public],
            write_access=[ACCESS_TYPE.user.format(self.authenticatedUser.id)],
            owner=self.authenticatedUser,
            is_featured=False
        )

        self.data = {
            "input": {
                "guid": self.discussionPublic.guid,
                "title": "My first Event",
                "description": "My description",
                "richDescription": "richDescription",
                "accessId": 0,
                "writeAccessId": 0,
                "tags": ["tag1", "tag2"],
                "isFeatured": True
            }
        }
        self.mutation = """
            fragment DiscussionParts on Discussion {
                title
                description
                richDescription
                timeCreated
                timeUpdated
                accessId
                writeAccessId
                canEdit
                tags
                url
                inGroup
                group {
                    guid
                }
                isFeatured
            }
            mutation ($input: editEntityInput!) {
                editEntity(input: $input) {
                    entity {
                    guid
                    status
                    ...DiscussionParts
                    }
                }
            }
        """

    def test_edit_event(self):

        variables = self.data

        request = HttpRequest()
        request.user = self.authenticatedUser

        result = graphql_sync(schema, { "query": self.mutation, "variables": variables }, context_value={ "request": request })

        data = result[1]["data"]

        self.assertEqual(data["editEntity"]["entity"]["title"], variables["input"]["title"])
        self.assertEqual(data["editEntity"]["entity"]["description"], variables["input"]["description"])
        self.assertEqual(data["editEntity"]["entity"]["richDescription"], variables["input"]["richDescription"])
        self.assertEqual(data["editEntity"]["entity"]["isFeatured"], False)

        self.discussionPublic.refresh_from_db()

        self.assertEqual(data["editEntity"]["entity"]["title"], self.discussionPublic.title)
        self.assertEqual(data["editEntity"]["entity"]["description"], self.discussionPublic.description)
        self.assertEqual(data["editEntity"]["entity"]["richDescription"], self.discussionPublic.rich_description)
        self.assertEqual(data["editEntity"]["entity"]["isFeatured"], False)

    def test_edit_event_editor(self):

        variables = self.data

        request = HttpRequest()
        request.user = self.editorUser

        result = graphql_sync(schema, { "query": self.mutation, "variables": variables }, context_value={ "request": request })

        data = result[1]["data"]

        self.assertEqual(data["editEntity"]["entity"]["title"], variables["input"]["title"])
        self.assertEqual(data["editEntity"]["entity"]["description"], variables["input"]["description"])
        self.assertEqual(data["editEntity"]["entity"]["richDescription"], variables["input"]["richDescription"])
        self.assertEqual(data["editEntity"]["entity"]["isFeatured"], variables["input"]["isFeatured"])

        self.discussionPublic.refresh_from_db()

        self.assertEqual(data["editEntity"]["entity"]["title"], self.discussionPublic.title)
        self.assertEqual(data["editEntity"]["entity"]["description"], self.discussionPublic.description)
        self.assertEqual(data["editEntity"]["entity"]["richDescription"], self.discussionPublic.rich_description)
        self.assertEqual(data["editEntity"]["entity"]["isFeatured"], self.discussionPublic.is_featured)
